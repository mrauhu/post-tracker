/*
  Node.js post tracker
  master process
 */

const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const zeus = require('./lib/zeus');

const HTTP_WORKER_PATH = './lib/http-worker.js';
const HTTP_WORKER_ALTERED_BEAST = typeof process.env.ALTERED_BEAST !== 'undefined'
  ? (process.env.ALTERED_BEAST === 'true')
  : true;

if (!cluster.isMaster) {
  return process.exit();
}
console.log('Master\t   ', process.pid, '\tstarted');

// Cluster setup master
cluster.setupMaster({
  exec: HTTP_WORKER_PATH
});

// Cluster fork http workers
for (let i = 0; i < numCPUs; i++) {
  cluster.fork();
}

cluster.on('fork', (worker) => {
  console.log('HTTP worker', worker.process.pid, '\trisen')
});

// Revive died http worker
cluster.on('exit', async (worker, code, signal) => {
  console.error('HTTP worker', worker.process.pid, '\tdied (', signal || code + ')');
  if (HTTP_WORKER_ALTERED_BEAST) {
    await zeus(cluster);
  }
});

process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error);
  console.trace();
});