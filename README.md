# post-tracker

> Example of the Node.js post tracking API

## Table of Contents

[TOC]

## Install

```bash
git clone https://bitbucket.org/mrauhu/post-tracker
cd post-tracker
```

## Usage

Server running in cluster mode, the number of workers is equal the number of CPU cores.

All requests to upstream servers are cached.

### Environment variables

* `HTTP_PATH_PREFIX`, by default `/tracking`
* `HTTP_PORT`, by default `3000`
* `HTTP_WORKER_ALTERED_BEAST` — revive died HTTP workers, by default `'true'`
* `CACHE_TTL_SECONDS`, by default `300`
* `CACHE_LRU_MAX_ITEMS`, by default `10240`


### Development server

Download [Node.js](https://nodejs.org).

Install dependencies:

```bash
npm install
```

Run server:

```bash
npm start
```

### Production server

Download Docker with [docker-compose](https://docs.docker.com/compose/install/).

Build container:

```bash
docker-compose build
```

Run server in background:

```bash
docker-compose up -d
```

## API

**Request**

`POST /tracking`

with JSON data:

```json
{
  "trackingNumber": "YOUR_TRACKING_NUMBER",
  "source": "NAME_OF_SERVICE"
}
```

**Response**

* `200 OK`

```json
{
    "status": "Success",
    "error": "",
    "general": {
        "sourceCountry": "China",
        "destinationCountry": "Russia"
    },
    "checkpoints": [
        {
            "time": "2017-09-02T10:46:00+03:00",
            "location": "Russia",
            "message": "Item delivered",
            "source": "sytrack"
        }
    ]
}
```
  
* `400 Bad Request`

```json
{
  "error": "Missing `source` or `trackingNumber`"
}
```

* `501 Not Implemented`

```json
{
  "error": "Service `NAME_OF_SERVICE` is missing"
}
```
  
* `502 Bad Gateway`

```json
{
  "error": "Invalid response from upstream server",
  "raw": "PARSED_JSON_FROM_UPSTREAM"
}
```

### Sypost.net

Source name: `sytrack`

Example of request via `cURL`:

```bash
curl --request POST \
  --url http://localhost:3000/tracking \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --data '{"trackingNumber": "YOUR_TRACKING_NUMBER", "source": "sytrack"}'
```

## Modularity

You can create a module for any service.

**Steps**

1\. Create new directory:

```bash
mkdir lib/services/DOMAIN_NAME_OF_SERVICE
```

2\. Create `index.js` inside:

```bash
cd lib/services/DOMAIN_NAME_OF_SERVICE
touch index.js
```
   
3\. Paste blueprint content to `index.js`:

```javascript
const fetch = require('node-fetch');
const FormData = require('form-data');

const NAME = 'NAME_OF_SERVICE'; // `source` param
const API_URL = 'SERVICE_API_URL';
const INPUT_NAME = 'FORM_INPUT_NAME';

/**
 * Track
 * @param code
 * @return {Promise<{status: number, response: *, error?: string, raw?: *}>}
 */
async function track (code) {
  const form = new FormData();
  form.append(INPUT_NAME, code);

  const result = await fetch(API_URL, {
    method: 'POST',
    body: form
  });

  const response = await result.json();

  if (result.status !== 200
    || typeof response.data === 'undefined') {
    // Bad gateway
    return {
      status: 502,
      response: {
        error: 'Invalid response from upstream server',
        raw: response
      }
    }
  }

  return {
    status: 200,
    response: response /*{
      ...convert(response),
      raw: response
    }*/
  }
}

module.exports[ NAME ] = track;
```

4\. Edit variables:

* `NAME` — `source` parameter in the request `POST /tracking`;
* `API_URL` — URL of upstream service;
* `INPUT_NAME` — form input name for `trackingCode`.
    
5\. Load new service in `lib/services/index.js`

```javascript
// Load available modules
// noinspection JSFileReferences
const name_of_service = require('./DOMAIN_NAME_OF_SERVICE');
// ...

// Joining all services to one object
// noinspection JSUnusedLocalSymbols
const services_obj = {
  ...name_of_service,
  // ...
};
```

6\. Now you can use the new service.

## Contribute

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Apache-2.0 © 2017 [Sergey N](https://mrauhu.ru)
