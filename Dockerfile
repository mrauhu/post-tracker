FROM mhart/alpine-node

RUN addgroup -S app && adduser -S -g app app

ENV HOME=/app
RUN mkdir -p $HOME

COPY package.json package-lock.json $HOME/
RUN chown -R app:app $HOME

WORKDIR $HOME

# Only for binary modules
#RUN apk add --no-cache make gcc g++ python libc6-compat && \
#  npm install --production  && \
#  apk del make gcc g++ python

RUN npm install --production


COPY . $HOME
RUN chown -R app:app $HOME
USER app

#CMD ["node", "index.js"]
