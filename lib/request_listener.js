/*
  Request listener
 */

const services = require('./services');

const HTTP_PATH_PREFIX = process.env.HTTP_PATH_PREFIX || '/tracking';

/**
 * HTTP request listener
 * @param req
 * @param res
 * @return {boolean|*|number}
 */
function request_listener (req, res) {
  res.setHeader('Content-Type', 'application/json; charset=utf-8');

  // Not found
  if (req.url !== HTTP_PATH_PREFIX) {
    res.writeHead(404);
    return res.end();
  }

  if (req.method !== 'POST') {
    if (req.method === 'OPTIONS') {
      // Send Options
      res.writeHead(200, {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'OPTIONS, POST'
      });
    } else {
      // Method not allowed
      res.writeHead(405);
    }
    return res.end();
  }

  /*
    POST
   */
  let json_string = '';
  req.on('data', (arrayBuffer) => {
    json_string += Buffer.from(arrayBuffer).toString();
  });

  req.on('end', async () => {
    // Minimum length based on `{}`
    if (json_string.length < 2) {
      // Bad request
      res.writeHead(400);
      return res.end();
    }

    let request;
    try {
      request = JSON.parse(json_string);
    } catch (e) {
      res.writeHead(400);
      return res.end();
    }
    console.log('request', request);

    const {status, response} = await services(request);

    res.writeHead(status);
    return res.end(
      JSON.stringify(response)
    );
  });

}

module.exports = request_listener;