const fetch = require('node-fetch');
const FormData = require('form-data');
const convert = require('./convert');

const API_URL = 'http://www.sypost.net/query';
const INPUT_NAME = 'connotNo';

/**
 * Track
 * @param code
 * @return {Promise<{status: number, response: *, error?: string, raw?: *}>}
 */
async function track (code) {
  const form = new FormData();
  form.append(INPUT_NAME, code);

  const result = await fetch(API_URL, {
    method: 'POST',
    body: form
  });

  const response = await result.json();

  if (result.status !== 200
    || typeof response.data === 'undefined') {
    // Bad gateway
    return {
      status: 502,
      response: {
        error: 'Invalid response from upstream server',
        raw: response
      }
    }
  }

  return {
    status: 200,
    response: convert(response) /*{
      ...convert(response),
      raw: response
    }*/
  }
}

module.exports = {
  sytrack: track
};