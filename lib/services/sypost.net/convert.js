/**
 * @typedef {{getCountry: Function}} iso3311a2
 */
const iso3311a2 = require('iso-3166-1-alpha-2');
const moment = require('moment');
require('moment-timezone');

const NAME = 'sytrack';
const TIMEZONE = 'Europe/Moscow';

/**
 * Convert
 * @param {Object} from
 * @param {number} from.status
 * @param {string} from.error
 * @param {Object[]} from.data
 * @param {string} from.data.$.orgCountry Origin country 2 letter code
 * @param {string} from.data.$.dstCountry Destination country 2 letter code
 * @return {Object}
 */
function convert (from) {
  // Way to lower a memory usage
  const to = Object.create(null);

  to.status = from.status === 1
    ? 'Success'
    : from.status;
  to.error = typeof from.error !== 'undefined'
    ? from.error
    : '';

  // Grab data
  const data = typeof from.data !== 'undefined'
  && Array.isArray(from.data)
  && from.data.length > 0
    ? from.data[0]
    : false;

  // Return raw answer if data is empty
  if (!data) {
    to.raw = from;
    return to;
  }

  // General
  to.general = Object.create(null);
  to.general.sourceCountry = iso3311a2.getCountry(data.orgCountry);
  to.general.destinationCountry = iso3311a2.getCountry(data.dstCountry);

  // Checkpoints
  to.checkpoints = convert_list(data);

  return to;
}

/**
 * Convert list of items
 * @param {Object} data
 * @param {Object} data.result
 * @param {Object} data.result.items
 * @param {Object[]} data.result.origin.items
 * @return {Array}
 */
function convert_list(data) {
  const items = data
    && typeof data.result !== 'undefined'
    && typeof data.result.origin !== 'undefined'
    && typeof data.result.origin.items !== 'undefined'
    && Array.isArray(data.result.origin.items)
  ? data.result.origin.items
  : false;

  if (!items) {
    return []
  }

  const list = [];
  for (let i = 0, ii = items.length; i < ii; i++) {
    list.push(
      convert_item(items[i])
    )
  }
  return list;
}

/**
 * Convert item
 * @param {Object} from_item
 * @param {string} from_item.timeZone Timezone, like +8
 * @param {number} from_item.createTime Unix time
 * @param {string} from_item.content
 * @param {string} from_item.office
 * @return {Object}
 */
function convert_item (from_item) {
  const to_item = Object.create(null);
  to_item.time = moment(from_item.createTime).tz(TIMEZONE).format();
  to_item.location = from_item.office
    ? from_item.office
    : '';
  to_item.message = from_item.content
    ? from_item.content
    : '';
  to_item.source = NAME;
  return to_item;
}

module.exports = convert;
