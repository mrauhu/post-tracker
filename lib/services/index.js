const cacheManager = require('cache-manager');
// Load available modules
const sypost = require('./sypost.net');

// Joining all services to one object
const services_obj = {
  ...sypost
};

// TTL in seconds
const CACHE_TTL_SECONDS = process.env.CACHE_TTL_SECONDS || 300;
// Max items, see: https://github.com/isaacs/node-lru-cache/issues/108
const CACHE_LRU_MAX_ITEMS = process.env.CACHE_LRU_MAX_ITEMS || 10240;

const cache = cacheManager.caching({
  store: 'memory',
  ttl: CACHE_TTL_SECONDS,
  max: CACHE_LRU_MAX_ITEMS
});

/**
 * Find service in services obj and make request
 * @param {Object} request
 * @param {string} request.source Name of service
 * @param {string} request.trackingNumber Code
 * @return {{status: number, response: {message?: '', error: ''}}}
 */
async function services ({source, trackingNumber}) {
  if (typeof source === 'undefined'
    || typeof trackingNumber === 'undefined') {
    return {
      status: 400,
      response: {
        error: 'Missing `source` or `trackingNumber`'
      }
    }
  }

  if (typeof services_obj[source] !== 'function') {
    return {
      status: 501,
      response: {
        error: 'Service `' + source + '` is missing'
      }
    }
  }
  // Caching every request to an upstream server
  const key = source + '_' + trackingNumber;
  return await cache.wrap(key, () => {
    console.log('caching key', key);
    return services_obj[source](trackingNumber);
  });
}

module.exports = services;