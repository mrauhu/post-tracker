/*
  HTTP worker
 */

// Using standard HTTP module for performance
const http = require('http');
const request_listener = require('./request_listener');

const HTTP_PORT = process.env.HTTP_PORT || 3000;

let server = http.createServer( request_listener );

server.listen(HTTP_PORT, () => {
  console.log('HTTP worker', process.pid, '\tlistening on port', HTTP_PORT);
});
