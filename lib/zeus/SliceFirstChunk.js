const Transform = require('stream').Transform;

/**
 * Slice first chunk of stream
 * @typedef {Transform|NodeJS.WritableStream} SliceFirstChunk
 */
class SliceFirstChunk extends Transform {
  /**
   *
   * @param options
   * @param {number} options.start
   */
  constructor(options) {
    super(options);

    this.start = typeof options.start === 'number'
      ? options.start
      : 0;
    if (this.start) {
      this.first_chunk = true;
    }
  }

  _transform(chunk, encoding, callback) {
    if (this.first_chunk) {
      chunk = chunk.slice(this.start);
      this.first_chunk = false;
    }
    this.push(chunk);
    callback();
  }
}

module.exports = SliceFirstChunk;