/**
 * Speaker
 * @type {EventEmitter|speaker}
 */

// Check if development dependencies installed
let Speaker;
try {
  Speaker = require.resolve('speaker') && require('speaker');
} catch (e) {}

module.exports = Speaker;