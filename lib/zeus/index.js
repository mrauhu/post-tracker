const fs = require('fs');
const zlib = require('zlib');
const SliceFirstChunk = require('./SliceFirstChunk');
const Speaker = require('./Speaker');

const ZEUS_VOICE_PATH = __dirname + '/../../src/zeus.gz';

/**
 * Zeus
 * @param {EventEmitter|*} lightning
 * @return {Promise<Worker, Error>}
 */
function zeus(lightning) {
  const altered_beast = typeof lightning !== 'undefined'
    ? lightning.fork()
    : false;

  return new Promise((resolve, reject) => {
    if (typeof Speaker === 'undefined') {
      return resolve(altered_beast);
    }

    // Remove WAV header that produced the click noise on start
    const cut44 = new SliceFirstChunk({
      start: 44
    });

    const speaker = new Speaker({
      channels: 1,
      bitDepth: 16,
      sampleRate: 48000,
    }).on('close', () => {
      resolve(altered_beast);
    })
      .on('error', reject);

    const gunzip = zlib.createGunzip().on('error', reject);

    const voice = fs.createReadStream(ZEUS_VOICE_PATH).on('error', reject);

    voice
      .pipe(gunzip)
      .pipe(cut44)
      .pipe(speaker);
  });
}

module.exports = zeus;
